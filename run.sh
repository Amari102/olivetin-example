#!/bin/sh
CONTAINER_NAME=my_script_interface_with_olivetin

# stop previous container if it exists
if [ "$( docker container inspect -f '{{.State.Running}}' "$CONTAINER_NAME" )" == "true" ]; then
    docker stop "$CONTAINER_NAME"
    while docker container inspect "$CONTAINER_NAME" >/dev/null 2>&1; do sleep 1; done
fi
# start new container
docker run -d --rm \
  --name "$CONTAINER_NAME" \
  -v ${PWD}/config.yaml:/etc/OliveTin/config.yaml:ro \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  -p 127.0.0.1:51337:1337 jamesread/olivetin
  echo "Running on http://localhost:51337"
